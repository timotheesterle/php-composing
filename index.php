﻿<?php 

if (($handle = fopen('./cs_figures.csv', 'r')) === false) {
    die('Erreur lors de l\'ouverture du fichier');
}

$headers = fgetcsv($handle, 1024, ',');
$data = [];

while ($row = fgetcsv($handle, 1024, ',')) {
    $data[] = array_combine($headers, $row);
}

fclose($handle);

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"
        />
        <title>Computer science figures</title>
    </head>
    <body>
        <div class="ui container">
            <h1>Computer science figures</h1>

            <div class="ui three column grid">
            <?php foreach ($data as $person) { ?>

            <div class="column">
                <a href=<?= "{$person["wikipedia"]}" ?>>
                    <div class="ui card">
                        <div class="image">
                            <img src=<?= "{$person["picture"]}" ?>
                            alt="card picture"/>
                        </div>
                        <div class="content">
                            <div class="header"><?= $person["name"] ?></div>
                            <div class="meta">
                                <span><?= $person["title"] ?></span>
                            </div>
                            <div class="description">
                                <?= $person["role"] ?>
                            </div>
                        </div>
                        <div class="extra content">
                            <span class="right floated">Born in
                                <?= $person["birthyear"] ?></span>
                        </div>
                    </div>
                </a>
            </div>

            <?php } ?>

            </div>
            <script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js"></script>
        </div>
    </body>
</html>
